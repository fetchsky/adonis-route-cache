"use strict";
const path = require("path");

module.exports = async function (cli) {
  try {
    await cli.makeConfig(
      "routeCache.js",
      path.join(__dirname, "./templates/config.js")
    );
    cli.command.completed("✔ create", "config/routeCache.js");
  } catch (error) {
    // ignore errors
  }
};
