"use strict";

const Config = use("Config");
/**@type {import('../Manager')} */
const RouteCache = use("Adonis/Src/RouteCache");

class RouteCacheMiddleware {
  constructor() {
    this._config = Config.get("routeCache");
  }
  async handle(ctx, next, cacheOptions) {
    if (!this._config.enable || ctx.request._isRouteCachedChecked) {
      return next();
    }
    ctx.request._isRouteCachedChecked = true;
    let isCacheableRequest = await RouteCache.isCacheableRequest(
      ctx,
      cacheOptions
    );
    if (!isCacheableRequest) {
      return next();
    }
    let cacheKey = await RouteCache.generateCacheKey(ctx, cacheOptions);
    let cachedResponse = await RouteCache.fetch(cacheKey);
    if (cachedResponse) {
      if (this._config.sendResponse) {
        return this._config.sendResponse(
          ctx,
          cachedResponse.statusCode,
          cachedResponse.content
        );
      }
      return ctx.response
        .status(cachedResponse.statusCode)
        .send(cachedResponse.content);
    }
    ctx.response.response.on("finish", () => {
      try {
        RouteCache.set(
          cacheKey,
          ctx.response.response.statusCode,
          ctx.response.lazyBody.content,
          cacheOptions.ttl
        );
      } catch (error) {
        console.error(error);
      }
    });
    return next();
  }
}

module.exports = RouteCacheMiddleware;
