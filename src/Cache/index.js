"use strict";

const Config = use("Adonis/Src/Config");
const routeCacheConfig = Config.get("routeCache");
const connectionConfig = routeCacheConfig[routeCacheConfig.connection];

if (!connectionConfig.driver) {
  throw Error("Please define driver for route cache");
}

var CacheDriver;

if (typeof connectionConfig.driver === "string") {
  if (["redis"].indexOf(connectionConfig.driver) >= 0) {
    CacheDriver = new (require("./" + connectionConfig.driver))(
      connectionConfig
    );
  } else {
    throw Error("Invalid driver name provided for route cache");
  }
} else if (
  typeof connectionConfig.driver.get === "function" &&
  typeof connectionConfig.driver.set === "function" &&
  typeof connectionConfig.driver.flush === "function"
) {
  CacheDriver = connectionConfig.driver;
} else {
  throw Error("Invalid driver provided for route cache");
}

module.exports = CacheDriver;
