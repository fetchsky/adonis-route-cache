"use strict";

const Redis = use("Redis");
const _isInteger = require("lodash/isInteger");

class RedisCache {
  constructor(driverConfig) {
    this._driverConfig = driverConfig;
  }

  get _redis() {
    return Redis.connection(this._driverConfig.connection);
  }

  /**
   * @param {String} key
   */
  async get(key) {
    return this._redis.get(key);
  }

  /**
   * @param {String} key
   * @param {String} data
   * @param {Number} [ttl] // In seconds
   */
  async set(key, data, ttl) {
    if (_isInteger(ttl)) {
      await this._redis.set(key, data, "EX", ttl);
    } else {
      await this._redis.set(key, data);
    }
  }

  /**
   * @param {String} pattern
   */
  async del(pattern) {
    return new Promise((resolve) => {
      var i = 0;
      var redisKeyPrefix = this._redis._config.keyPrefix;
      var stream = this._redis.scanStream({
        match: redisKeyPrefix + pattern + "*",
      });
      stream.on("data", (keys) => {
        // `keys` is an array of strings representing key names
        if (keys.length) {
          var pipeline = this._redis.pipeline();
          keys.forEach((key) => {
            i++;
            pipeline.del(key.replace(redisKeyPrefix, ""));
          });
          return pipeline.exec();
        }
      });
      stream.on("end", () => {
        resolve(i);
      });
    });
  }
}

module.exports = RedisCache;
