"use strict";

const moment = use("moment");
const { Command } = use("@adonisjs/ace");
/**@type {import('../Manager')}*/
const RouteCache = use("Adonis/Src/RouteCache");

class RouteCacheFlush extends Command {
  /**
   * Signature of the console command.
   *
   * @returns {String}
   */
  static get signature() {
    return "route-cache:flush";
  }

  /**
   * Description of the console command.
   *
   * @returns {String}
   */
  static get description() {
    return "Flush all routes cache";
  }

  /**
   * Execute the console command.
   *
   * @returns {Promise}
   */
  async handle() {
    try {
      var startTime = moment();
      var totalDeletions = await RouteCache.flush();
      if (totalDeletions) {
        const totalTime =
          moment.duration(moment().diff(startTime)).asMilliseconds() + " ms";
        this.success(
          `${this.icon(
            "success"
          )} Cache flushed successfully. Time taken: ${totalTime}`
        );
      } else {
        this.success(`${this.icon("success")} No cache found.`);
      }
      process.exit(0);
    } catch (err) {
      console.error(err);
      this.error(`${this.icon("error")} Failed to flush route cache`);
      process.exit(1);
    }
  }
}

module.exports = RouteCacheFlush;
