"use strict";

const moment = use("moment");
const { Command } = use("@adonisjs/ace");
/**@type {import('../Manager')}*/
const RouteCache = use("Adonis/Src/RouteCache");

class RouteCacheDel extends Command {
  /**
   * Signature of the console command.
   *
   * @returns {String}
   */
  static get signature() {
    return "route-cache:del {api: Api path including prefix} {method?: Specify only if you want to delete cache against specific method (GET, POST, etc.)}";
  }

  /**
   * Description of the console command.
   *
   * @returns {String}
   */
  static get description() {
    return "Deletes cache against specific route";
  }

  /**
   * Execute the console command.
   *
   * @param {Object} args
   * @returns {Promise}
   */
  async handle(args) {
    try {
      var startTime = moment();
      var totalDeletions = await RouteCache.del(args.api, args.method);
      if (totalDeletions) {
        const totalTime =
          moment.duration(moment().diff(startTime)).asMilliseconds() + " ms";
        this.success(
          `${this.icon(
            "success"
          )} Cache deleted successfully. Time taken: ${totalTime}`
        );
      } else {
        this.success(
          `${this.icon("success")} No cache found against this route.`
        );
      }
      process.exit(0);
    } catch (err) {
      console.error(err);
      this.error(`${this.icon("error")} Failed to delete route cache`);
      process.exit(1);
    }
  }
}

module.exports = RouteCacheDel;
