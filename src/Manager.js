"use strict";

var cachedRoutes;
const crypto = require("crypto");
const _get = require("lodash/get");
const _isEmpty = require("lodash/isEmpty");
const _isArray = require("lodash/isArray");
const Helpers = use("Adonis/Src/Helpers");

class RouteCacheManager {
  constructor(config) {
    this._config = config.get("routeCache");
    /**@type {import('./Cache/redis')}*/
    this._cache = require("./Cache");
  }
  get keyPrefix() {
    return "$route$:";
  }
  /**
   * Checks if request can be cached or not
   *
   * @param {*} ctx
   * @param {Object} options
   * @param {Object} [options.ignore]
   * @param {String[]} [options.ignore.body]
   * @param {String[]} [options.ignore.query]
   * @param {String[]} [options.ignore.headers]
   * @param {Function} [options.ignore.rule]
   */
  async isCacheableRequest(ctx, options) {
    let shouldCache = true;
    if (options.ignore) {
      shouldCache = await isCacheableRequest(ctx, options.ignore);
    }
    if (
      shouldCache &&
      !_get(options, "ignore.overrideGlobal") &&
      this._config.global.ignore
    ) {
      shouldCache = await isCacheableRequest(ctx, this._config.global.ignore);
    }
    return shouldCache;
  }
  /**
   * Generates cache key
   *
   * @param {*} ctx
   * @param {Object} options
   * @param {Object} [options.dynamic]
   * @param {String[]} [options.dynamic.body]
   * @param {String[]} [options.dynamic.query]
   * @param {String[]} [options.dynamic.headers]
   * @param {Function} [options.dynamic.rule]
   */
  async generateCacheKey(ctx, options) {
    let cacheKey = ctx.request.url();
    if (options.key && typeof options.key === "string") {
      cacheKey = options.key;
    }
    cacheKey = ctx.request.method() + ":" + cacheKey + "$";
    let dynamicParams = {};
    if (
      _get(this._config, "global.dynamic") &&
      !_get(options, "dynamic.overrideGlobal")
    ) {
      dynamicParams = await getDynamicParams(
        ctx,
        this._config.global.dynamic,
        dynamicParams
      );
    }
    if (options.dynamic) {
      dynamicParams = await getDynamicParams(
        ctx,
        options.dynamic,
        dynamicParams
      );
    }
    if (_isEmpty(dynamicParams)) {
      return cacheKey;
    }
    let dynamicKey = crypto
      .createHash("sha256")
      .update(JSON.stringify(dynamicParams), "utf8")
      .digest()
      .toString("hex");
    return cacheKey + ":dn" + dynamicKey;
  }
  /**
   * Sets route cache
   *
   * @param {String} cacheKey
   * @param {Number} statusCode
   * @param {*} content
   * @param {Number} [params.ttl]
   */
  async set(cacheKey, statusCode, content, ttl) {
    if (!ttl) {
      ttl = this._config.global.ttl;
    }
    let response = {
      statusCode,
      content: JSON.stringify(content) || "''",
    };
    await this._cache.set(
      this.keyPrefix + cacheKey,
      JSON.stringify(response),
      ttl
    );
  }
  /**
   * Fetches route cache
   *
   * @param {String} cacheKey
   */
  async fetch(cacheKey) {
    let response = await this._cache.get(this.keyPrefix + cacheKey);
    if (response) {
      try {
        response = JSON.parse(response);
        response.content = JSON.parse(response.content);
      } catch (error) {
        console.error(error);
      }
    }
    return response;
  }
  /**
   * Delete all cache against specific route
   *
   * @param {String} url
   * @param {String} [method='*']
   */
  async del(url, method) {
    if (!method) {
      method = "*";
    }
    return this._cache.del(
      this.keyPrefix + method.toUpperCase() + ":" + url + "$*"
    );
  }
  /**
   * Flushes all route cache
   */
  async flush() {
    return this._cache.del(this.keyPrefix);
  }
}

/**
 * Checks if request can be cached or not
 *
 * @param {*} ctx
 * @param {Object} ignoreOptions
 * @param {String[]} [ignoreOptions.body]
 * @param {String[]} [ignoreOptions.query]
 * @param {String[]} [ignoreOptions.headers]
 * @param {Function} [ignoreOptions.rule]
 */
async function isCacheableRequest(ctx, ignoreOptions) {
  if (_isArray(ignoreOptions.body)) {
    let bodyParams = ctx.request.post();
    for (let i = 0; i < ignoreOptions.body.length; i++) {
      let bodyKey = ignoreOptions.body[i];
      if (typeof _get(bodyParams, bodyKey) !== "undefined") {
        return false;
      }
    }
  }
  if (_isArray(ignoreOptions.query)) {
    let queryParams = ctx.request.get();
    for (let i = 0; i < ignoreOptions.query.length; i++) {
      let queryKey = ignoreOptions.query[i];
      if (typeof _get(queryParams, queryKey) !== "undefined") {
        return false;
      }
    }
  }
  if (_isArray(ignoreOptions.headers)) {
    let headers = ctx.request.headers();
    for (let i = 0; i < ignoreOptions.headers.length; i++) {
      let headerKey = ignoreOptions.headers[i];
      if (typeof _get(headers, headerKey) !== "undefined") {
        return false;
      }
    }
  }
  if (typeof ignoreOptions.rule === "function") {
    let ignoreCache = await ignoreOptions.rule(ctx);
    return !ignoreCache;
  }
  return true;
}

/**
 *
 * Get dynamic params
 *
 * @param {*} ctx
 * @param {Object} dynamicOptions
 * @param {String[]} [dynamicOptions.body]
 * @param {String[]} [dynamicOptions.query]
 * @param {String[]} [dynamicOptions.headers]
 * @param {Function} [dynamicOptions.rule]
 */
async function getDynamicParams(ctx, dynamicOptions, initialValue = {}) {
  if (_isArray(dynamicOptions.body) && dynamicOptions.body.length) {
    let bodyParams = ctx.request.post();
    for (let i = 0; i < dynamicOptions.body.length; i++) {
      let bodyKey = dynamicOptions.body[i];
      let bodyValue = _get(bodyParams, bodyKey);
      if (typeof bodyValue !== "undefined") {
        initialValue["b_" + bodyKey] = bodyValue;
      }
    }
  }
  if (_isArray(dynamicOptions.query) && dynamicOptions.query.length) {
    let queryParams = ctx.request.get();
    for (let i = 0; i < dynamicOptions.query.length; i++) {
      let queryKey = dynamicOptions.query[i];
      let queryValue = _get(queryParams, queryKey);
      if (typeof queryValue !== "undefined") {
        initialValue["q_" + queryKey] = queryValue;
      }
    }
  }
  if (_isArray(dynamicOptions.headers) && dynamicOptions.headers.length) {
    let headers = ctx.request.headers();
    for (let i = 0; i < dynamicOptions.headers.length; i++) {
      let headerKey = dynamicOptions.headers[i];
      let headerValue = _get(headers, headerKey);
      if (typeof headerValue !== "undefined") {
        initialValue["h_" + headerKey] = headerValue;
      }
    }
  }
  if (typeof dynamicOptions.rule === "function") {
    let dynamicRuleValue = await dynamicOptions.rule(ctx);
    if (!initialValue.r_1) {
      initialValue.r_1 = dynamicRuleValue;
    } else {
      initialValue.r_2 = dynamicRuleValue;
    }
  }
  return initialValue;
}

module.exports = RouteCacheManager;
