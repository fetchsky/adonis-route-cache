# Adonis Route Cache

## Installation

`$ adonis install https://gitlab.com/fetchsky/adonis-route-cache.git --as adonis-route-cache`

## Registering provider & commands

Make sure to register this plugin's provider & commands. The providers and commands are registered inside `start/app.js` file.

```js
const providers = ["adonis-route-cache/providers/RouteCacheProvider"];
const commands = [
  "Adonis/Commands/RouteCache:Del",
  "Adonis/Commands/RouteCache:Flush",
];
```

## Delete Cache For Single Route

Execute the following command to delete all cache against single route.
`$ adonis route-cache:del YOUR_ROUTE_PATH`

## Flush Cache

Execute the following command to flush cache against all routes.
`$ adonis route-cache:flush`
