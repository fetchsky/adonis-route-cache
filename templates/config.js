"use strict";

const Env = use("Env");

module.exports = {
  /*
  |--------------------------------------------------------------------------
  | Enable/Disable
  |--------------------------------------------------------------------------
  |
  | Here we define whether we want to use cache or not
  |
  */
  enable: Env.get("ROUTE_CACHE_ENABLE", "true") === "true",

  /*
  |--------------------------------------------------------------------------
  | Cache connection
  |--------------------------------------------------------------------------
  |
  | Cache connection to be used.
  |
  */
  connection: Env.get("ROUTE_CACHE_CONNECTION", "redis"),

  /*
  |--------------------------------------------------------------------------
  | Redis connection
  |--------------------------------------------------------------------------
  |
  | Here we define connection settings for redis cache connection.
  |
  | Make sure you have installed redis package for adonisjs.
  | https://adonisjs.com/docs/4.x/redis
  |
  */
  redis: {
    driver: "redis",
    connection: Env.get("REDIS_CONNECTION", "primary"),
  },

  /*
  |--------------------------------------------------------------------------
  | Global Options
  |--------------------------------------------------------------------------
  |
  */
  global: {
    /*
    |--------------------------------------------------------------------------
    | Default TTL
    |--------------------------------------------------------------------------
    | 
    | You can also define different ttl for each route when calling `.cache()`
    | method.
    */
    ttl: 86400, // In seconds

    /*
    |--------------------------------------------------------------------------
    | Ignore Cache
    |--------------------------------------------------------------------------
    |
    | Here we will define options to ignore cache. 
    | You can also define different set of options for each route when calling
    | `.cache()` method. The options defined here will be checked first.
    |
    */
    //  ignore: {
    //    body: [],
    //    query: [],
    //    headers: [],
    //    rule: async function (ctx) {
    //      // Return `true` to ignore cache
    //    },
    //  },

    /*
    |--------------------------------------------------------------------------
    | Dynamic Cache
    |--------------------------------------------------------------------------
    |
    | Here we will define all the options that can impact cache. 
    | These options will be used to generate dynamic keys for route.
    | You can also define different set of options for each route when calling
    | `.cache()` method. The options defined here will automatically be included
    | in route-wise dynamic options.
    |
    */
    //  dynamic: {
    //    body: [],
    //    query: [],
    //    headers: [],
    //    rule: async function (ctx) {
    //      // Return `string`
    //    },
    //  },
  },

  /*
  |--------------------------------------------------------------------------
  | Response Utility
  |--------------------------------------------------------------------------
  |
  | We will be using adonis default response utility to send response.
  |
  | You can customize this behaviour by specifying custom function for sending response.
  | 
  | E.g: sendResponse: (ctx, status, content)
  |
  */
  sendResponse: null,
};
