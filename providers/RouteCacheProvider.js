"use strict";

const { ServiceProvider } = use("@adonisjs/fold");

class RouteCacheProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
    // Registering Middleware
    this.app.singleton("Adonis/Src/RouteCache", (app) => {
      const Config = use("Adonis/Src/Config");
      const CacheManager = require("../src/Manager");
      return new CacheManager(Config);
    });
    // Registering Middleware
    this.app.bind("Adonis/Middleware/RouteCache", (app) => {
      const CacheMiddleware = require("../src/Middleware/RouteCache");
      return new CacheMiddleware();
    });
    // // Registering Commands
    this.app.bind("Adonis/Commands/RouteCache:Del", () => {
      return require("../src/Commands/Del");
    });
    this.app.bind("Adonis/Commands/RouteCache:Flush", () => {
      return require("../src/Commands/Flush");
    });
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
    const CacheMiddleware = use("Adonis/Middleware/RouteCache");
    const Route = use("Adonis/Src/Route");
    Route.Route.macro("cache", function (options) {
      if (!options) {
        options = {};
      }
      this.middleware(async function (ctx, next) {
        return CacheMiddleware.handle(ctx, next, options);
      });
      return this;
    });
    Route.RouteGroup.macro("cache", function (options) {
      if (!options) {
        options = {};
      }
      this.middleware(async function (ctx, next) {
        return CacheMiddleware.handle(ctx, next, options);
      });
      return this;
    });
  }
}

module.exports = RouteCacheProvider;
